# Visualiseur des données de l'Observatoire de l'Open Data

Widget **[Datami](https://gitlab.com/multi-coop/datami)** de prévisualisation des données issues de fichiers csv de l'Observatoire de l'Open Data - Open Data France.

---

## Résumé

Open Data France possède un site (https://www.observatoire-opendata.fr/) visant à mesurer le déploiement de l'open data en France, à sensibiliser les collectivitées et les acteurs publics à l'open data, et à valoriser ces données sous formes de diverses visualisations . Sont référencés sur ce site différents types de données : organisations, plateformes, etc...

Afin de pouvoir partager et de mettre en valeur toutes ces ressources il a été proposé d'utiliser le _widget open source_ créé par la coopérative numérique **[multi](https://multi.coop)** : le projet **[Datami](https://gitlab.com/multi-coop/datami)**.

Cet outil permet la **visualisation** des données mais également la **contribution**.

**Datami** permet d'intégrer sur des sites tiers (sites de partenaires ou autres) une sélection plus ou moins large de ressources. Cette solution permet à la fois d'éviter aux sites partenaires de "copier-coller" les ressources, d'afficher sur ces sites tiers les ressources toujours à jour, et de permettre aux sites tiers ainsi qu'au site source de gagner en visibilité, en légitimité et en qualité d'information.

L'autre avantage de cette solution est qu'elle n'est déployée qu'une fois, mais que le widget peut être intégré et paramétré/personnalisé sur autant de sites tiers que l'on souhaite... **gratuitement**.

A titre de démonstration de généricité de cette solution de widget nous l'avons déclinée de différentes manières, par exemple en configurant et intégrant ce widget pour qu'il affiche des "communs" référencés sur le wiki [Résilience des territoires](https://wiki.resilience-territoire.ademe.fr).

---

## Démo

- Page html de démo : [![Netlify Status](https://api.netlify.com/api/v1/badges/854fd222-efeb-4f27-a879-0484bcd8af69/deploy-status)](https://app.netlify.com/sites/datami-demo-odf-data-observatoire/deploys)
- url de démo :
  - DEMO / données observatoire ODF : https://datami-demo-odf-data-observatoire.netlify.app/

---

### Documentation

Un site dédié à la documentation technique de Datami est consultable ici : https://datami-docs.multi.coop

---

## Crédits

| | logo | url |
| :-: | :-: | :-: |
| **Open Data France** | ![ODF](./images/odf-logo.svg) | https://www.opendatafrance.net/ |
| **coopérative numérique multi** | ![multi](./images/multi-logo.png) | https://multi.coop |

---

## Pour aller plus loin... 

### Datami

Le widget fait partie intégrante du projet [Datami](https://gitlab.com/multi-coop/datami)

---

## Mini server for local development

A mini server is writen in the `server.py` to serve this folder's files

To install the mini-server :

```sh
pip install --upgrade pip
python3 -m pip install --user virtualenv
python3 -m venv venv
source venv/bin/activate
pip install --upgrade pip
pip install -r requirements.txt
```

or

```sh
sh setup.sh
source venv/bin/activate
```

### Run local server

To run the server on `http://localhost:8800`:

```sh
python server.py
```

or

```sh
sh run_server.sh
```

The html file `index.html` will be served on `http://localhost:8800`

Files will be locally served on :

- `http://localhost:8800/content/<path:folder_path>/<string:filename>`
- `http://localhost:8800/statics/<path:folder_path>/<string:filename>`


### Effectuer une mise à jour des données à partir d'un nouveau dataset sous format csv

Le script `compile_data_source_file.py` permet de transformer un fichier csv de données source en un fichier utilisable et viualisable par cette instance de datami
A partir du fichier csv des données source téléchargé depuis [Airtable](https://airtable.com/shrKrV6KY7BlhHDx7), ce script génère un fichier csv formaté et 
directement exploitable par [carto.observatoires-opendata.fr](https://carto.observatoire-opendata.fr/).

Pour calculer les adresses, le script se base sur les données de longitude et latitude renseignées dans le fichier de données source, 
lequel est téléchargé depuis Airtable.

Si celles-ci ne sont pas renseignées dans les données sources, le script utilise l'[API Sirene](https://api.insee.fr/catalogue/site/themes/wso2/subthemes/insee/pages/item-info.jag?name=Sirene&version=V3&provider=insee#/) 
et l'[API de la Base Adresse Nationale](https://adresse.data.gouv.fr/api-doc/adresse) pour calculer les longitudes, latitudes et adresses.

Une option dans le script permet également de récupérer ces données depuis un "run" antérieur de mise à jour des données 
si celles-ci ne sont pas renseignées dans les données sources d'Airtable. Cette option permet notamment de réduire le temps 
de traitement des données et évite d'avoir à recalculer à chaque "run" des informations déjà récupérées précédemment.

Pour regénérer les données "consommées" à partir des données source d'Airtable :
```sh
python compile_data_source_file.py 'data/csv/source/Organisations-Annuaire des Organisations.csv' -token $api_sirene_token -debug y
```

Pour mettre à jour les données "consommées" à partir des données "source" avec vérification préalable des champs 
déjà renseignés dans les données "consommées" générées auparavant dans le dernier run, il faut renseigner le path du fichier 
output du dernier run en argument -last_run_file, par exemple :
```sh
python compile_data_source_file.py 'data/csv/source/Organisations-Annuaire des Organisations.csv' -last_run_file 'data/csv/output/runs/Organisations-Annuaire des Organisations_20230302-185514.csv' -token $api_sirene_token -debug y
```

L'argument `token` correspond au jeton d'accès à l'[api sirene](https://api.insee.fr/catalogue/site/themes/wso2/subthemes/insee/pages/item-info.jag?name=Sirene&version=V3&provider=insee) 
cela nécessite la création d'un compte sur cet api pour obtenir un jeton d'acès à l'api.

Une fois que le script se termine, un nouveau fichier de sortie `Organisations-Annuaire des Organisations.csv` avec les nouvelles 
données de sortie est automatiquement créé dans le répertoire `data/csv/output/`.
Une copie du fichier de sortie est également automatiquement sauvegardée dans le répertoire `data/csv/output/runs/`
avec le même nom de fichier concaténé à la date et l'heure de création du fichier, par exemple : `Organisations-Annuaire des Organisations-20230228-1207.csv`


## Suites à donner : Mise en place un pipeline d’intégration continue (CI)
### Objectifs : 
1) automatiser à intervalles réguliers l’interrogation des données sources sur Airtable
2) effectuer la mise à jour des données si des modifications ont été apportées aux données sources (sur la base du script livré aujourd'hui).
    - Ce développement permettrait une automatisation complète et rapide des données affichées dans la cartographie, sans avoir besoin d'être prévenu lors des mises à jour dans Airtable.
    - Ce développement pourrait par ailleurs être mutualisé avec le travail fait sur le 1er livrable de D-Lyne, le "fichier miroir".

### TODO list :
- Améliorer le script `compile_data_source_file.py` en vue d'une intégration continue :
  - Récupérer les données source directement depuis l'API d'Airtable sans avoir à téléchargé le fichier source depuis Airtable : 
    - utiliser la fonction `extract` de la classe `ODF` issue du [dépôt gitlab observatoire-odf](https://git.opendatafrance.net/observatoire/observatoire-odf/-/blob/main/scripts/etl.py#L143) 
    - cela nécessite de récupérer les accès (key et id) du fichier Airtable des données de l'Observatoire ODT
  - Calculer les données de longitude, latitude et adresse directement par le biais de l'API Recherche d'Entreprises comme évoqué dans ce [commentaire](https://gitlab.com/multi-coop/datami-project/datami-clients/datami-odf-observatoire/-/issues/1#note_1306075727)
    (cela permettra de n'avoir recours qu'à une seule API sans token d'accès)
- Mettre en place une action gitlab qui lancerait le script automatiquement à intervalles réguliers 

