import argparse
import datetime
import time
import pandas as pd
from geopy.geocoders import BANFrance
import requests


def define_tranche_pop(pop_insee: int) -> str:
    if pop_insee is None or pop_insee == '':
        return "p_na"
    elif 0 <= pop_insee <= 999:
        return "p_0_1"
    elif 1000 <= pop_insee <= 4999:
        return "p_1_5"
    elif 5000 <= pop_insee <= 9999:
        return "p_5_10"
    elif 10000 <= pop_insee <= 24999:
        return "p_10_25"
    elif 25000 <= pop_insee <= 49999:
        return "p_25_50"
    elif 50000 <= pop_insee <= 99999:
        return "p_50_100"
    elif 100000 <= pop_insee <= 249999:
        return "p_100_250"
    elif 250000 <= pop_insee <= 499999:
        return "p_250_500"
    elif 500000 <= pop_insee <= 999999:
        return "p_500_1000"
    else:  # pop_insee >= 1 000 000
        return "p_1000_5000.001"


def define_tranche_ptf(nb_ptf: int) -> str:
    if nb_ptf is None or nb_ptf == '' or nb_ptf == 0:
        return "d_na"
    elif 1 <= nb_ptf <= 5:
        return "d_1_5"
    elif 6 <= nb_ptf <= 10:
        return "d_6_10"
    elif 11 <= nb_ptf <= 25:
        return "d_11_25"
    elif 26 <= nb_ptf <= 50:
        return "d_26_50"
    elif 51 <= nb_ptf <= 100:
        return "d_51_100"
    elif 101 <= nb_ptf <= 250:
        return "d_101_250"
    elif 251 <= nb_ptf <= 500:
        return "d_251_500"
    else:  # nb_ptf > 500
        return "d_501_5000"


MANY_ADRESSES_MESSAGE = 'ATTENTION il y a plusieurs établissements siège pour ce numéro de SIREN'


def compile_address_from_etablissement(etablissement_siege: list) -> str:
    if len(etablissement_siege) != 1:
        address_concat = MANY_ADRESSES_MESSAGE
    else:
        etablissement_siege[0]['adresseEtablissement'].pop('codeCommuneEtablissement')
        address_list = []
        for field in list(etablissement_siege[0]['adresseEtablissement'].values()):
            if not field is None:
                address_list.append(field)
        address_concat = ' '.join(address_list)
    return address_concat


def get_address(siren: int, token: str, lon: float, lat: float) -> str:
    try:
        # Get address from siren
        url = f"https://api.insee.fr/entreprises/sirene/V3/siret?q=siren:{siren}"
        req = requests.get(url, headers={'Authorization': f'Bearer {token}'}, timeout=10)
        res = req.json()

        etablissement_siege = [x for x in res['etablissements'] if x['etablissementSiege']]
        if len(etablissement_siege) == 0:
            raise Exception("Aucun établissement siège n'est associé à ce numéro de siren")
        else:
            address = compile_address_from_etablissement(etablissement_siege)

    except Exception as e:
        # Get address from lon and lat
        #  NB : Pour un point gps donnée, méthode retourne plusieurs adresses. Arbitrairement on choisit de retourner
        #  la première adresse de la réponse à la requête qui est censée êtr ela plus pertinente.
        if lon == '' or lat == '':
            return ''
        else:
            url = f"https://api-adresse.data.gouv.fr/reverse/?lon={lon}&lat={lat}"
            req = requests.get(url)
            try:
                res = req.json()
                address = res['features'][0]['properties']['label']
            except Exception:
                address = ''

    return address


def get_address_from_last_run(siren: int, last_run_file: str) -> str:
    df_last_run = pd.read_csv(last_run_file)
    row = df_last_run.loc[df_last_run["siren"] == f'{siren}']
    try:
        return row['adress'].values[0]
    except Exception:
        return ''


def get_lon_lat_from_last_run(siren: int, last_run_file: str) -> (float, float):
    df_last_run = pd.read_csv(last_run_file)
    row = df_last_run.loc[df_last_run["siren"] == f'{siren}']
    try:
        return row['long'].values[0], row['lat'].values[0]
    except Exception:
        return None, None


def get_lon_lat_from_address(address: str) -> (float, float):
    if address == '':
        return None, None
    elif address == MANY_ADRESSES_MESSAGE:
        return None, None
    else:
        geocoder = BANFrance()
        try:
            location = geocoder.geocode(address, exactly_one=True, timeout=30)
            return location.longitude, location.latitude
        except Exception:
            return None, None


def get_region_and_dep_label_from_departement(departement: int) -> (str, str, str):
    dep_reg_file = 'data/csv/referentiel/dep_to_reg.csv'
    df_dep_reg = pd.read_csv(dep_reg_file)
    df_dep_reg["dep_code"] = df_dep_reg.apply(lambda x:
                                              x["dep_code"].zfill(2) if (
                                                      x["dep_code"] != '2A' or x["dep_code"] != '2B') else x[
                                                  "dep_code"],
                                              axis=1)
    row = df_dep_reg.loc[df_dep_reg["dep_code"] == f'{departement}']

    # Add reg_code_short
    try:
        reg_code = row['reg_code'].values[0]
    except Exception:
        # TODO : traiter les cas où departement = 37,41 ou 77,91 ou ''
        reg_code = ''

    reg_code_short = reg_code.split('-')[0]

    # Add reg_code_geo
    try:
        reg_code_geo = row['reg_code_num'].values[0]
    except Exception:
        reg_code_geo = ''

    # Add dep_label
    try:
        dep_label = row['dep_name'].values[0]
    except Exception:
        dep_label = ''

    return reg_code_short, reg_code_geo, dep_label


if __name__ == '__main__':

    start = time.time()

    parser = argparse.ArgumentParser(description="Transform a csv dataset of carte-observatoire-odf to visualize it "
                                                 "in datami",
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("-v", "--verbose", action="store_true", help="increase verbosity")
    parser.add_argument("src", help="Path to source dataset csv file to transform")
    parser.add_argument("-last_run_file", help="Use last run file to update data", default='')
    parser.add_argument("-token", type=str, help="Access token to the api sirene", default="")
    parser.add_argument("-debug", "--debug", type=bool, help="Debugging", default=False)

    args = parser.parse_args()

    OUTPUT_PATH = "data/csv/output/"

    OUTPUT_FILE_NAME = args.src.split('/')[-1].split('.')[0]

    # Transform data source file csv into a Dataframe pandas object
    df_src = pd.read_csv(args.src)

    if args.debug:
        print("Transform dataset ...")

    # Delete unused columns
    df_expected = df_src.drop(["A vériifier", "TimeStamp"], axis=1)

    # Rename columns
    df_expected.rename(columns={'url-ptf': 'url_ptf', 'nb-ptf': 'nb_ptf',
                                'url-datagouv': 'url_datagouv', 'nb-datagouv': 'nb_datagouv',
                                'id-datagouv': 'id_datagouv', 'id-ods': 'id_ods',
                                'pop-insee': 'pop_insee', 'Thèmatiques': 'Thématiques', 'ptf': 'orga'},
                       inplace=True)

    # Replace nan values with empty str
    df_expected.fillna('', inplace=True)

    # Add column tranche_pop
    df_expected['tranche_pop'] = df_expected.apply(lambda x: define_tranche_pop(x['pop_insee']), axis=1)

    # Add column tranche_ptf
    df_expected['tranche_ptf'] = df_expected.apply(lambda x: define_tranche_ptf(x['nb_ptf']), axis=1)

    # Replace ',' by '-' in 'Thématiques' and 'Techno (from Plateformes)' and 'orga' columns
    df_expected['Techno (from Plateformes)'] = df_expected.apply(lambda x: x['Techno (from Plateformes)'].replace(
        ',', ' -'), axis=1)
    df_expected['Thématiques'] = df_expected.apply(lambda x: x['Thématiques'].replace(',', ' -'), axis=1)
    df_expected['orga'] = df_expected.apply(lambda x: x['orga'].replace(',', ' - '), axis=1)

    # Add reg_code_short, reg_code_geo and dep_label columns
    if args.debug:
        print("Add reg_code_short, reg_code_geo and dep_label columns ...")
    df_expected['reg_code_short'] = df_expected.apply(
        lambda x: get_region_and_dep_label_from_departement(x['Département'])[0],
        axis=1)
    df_expected['reg_code_geo'] = df_expected.apply(
        lambda x: get_region_and_dep_label_from_departement(x['Département'])[1], axis=1)
    df_expected['dep_label'] = df_expected.apply(
        lambda x: get_region_and_dep_label_from_departement(x['Département'])[2], axis=1)

    # Get longitude and latitude from last run if are unknown
    if args.last_run_file != '':
        if args.debug:
            print("Get unknown longitude and latitude from last run ...")
        df_expected['long'] = df_expected.apply(
            lambda x: get_lon_lat_from_last_run(x['siren'], args.last_run_file)[0] if x['long'] == '' else x['long'],
            axis=1)
        df_expected['lat'] = df_expected.apply(
            lambda x: get_lon_lat_from_last_run(x['siren'], args.last_run_file)[1] if x['lat'] == '' else x['lat'],
            axis=1)

    # Add column adresse
    if args.debug:
        print("Add address column ...")
    # Get address
    if args.last_run_file != '':
        # Get address from last run
        if args.debug:
            print("Get address from last run ...")
        df_expected['adresse'] = df_expected.apply(lambda x: get_address_from_last_run(x['siren'], args.last_run_file),
                                                   axis=1)
    else:
        # Get address from sirene or from longitude and latitude
        if args.debug:
            print("Get address from siren or from longitude and latitude ...")
        df_expected['adresse'] = df_expected.apply(lambda x: get_address(x['siren'], args.token, x['long'], x['lat']),
                                                   axis=1)

    # Get address from siren or longitude and latitude if address is still unknown (if use last run)
    if args.last_run_file:
        if args.debug:
            print("Get address from siren or longitude and latitude if address is still unknown ...")

        df_expected['adresse'] = df_expected.apply(lambda x:
                                                   get_address(x['siren'], args.token, x['long'], x['lat'])
                                                   if x['adresse'] == '' else x['adresse'], axis=1)

    # Get longitude and latitude from address if are still unknown
    if args.debug:
        print("Get unknown longitude and latitude from address ...")
    df_expected.fillna('', inplace=True)
    df_expected['long'] = df_expected.apply(
        lambda x: get_lon_lat_from_address(x['adresse'])[0] if x['long'] == '' else x['long'],
        axis=1)
    df_expected['lat'] = df_expected.apply(
        lambda x: get_lon_lat_from_address(x['adresse'])[1] if x['lat'] == '' else x['lat'],
        axis=1)

    # Change columns order
    df_expected = df_expected[['siren', 'nom', 'type', 'adresse', 'url_ptf', 'nb_ptf', 'url_datagouv', 'nb_datagouv',
                               'merge', 'tranche_ptf', 'Techno (from Plateformes)', 'orga', 'Thématiques', 'id_ods',
                               'Département', 'dep_label', 'reg_code_short', 'reg_code_geo', 'pop_insee', 'tranche_pop',
                               'lat', 'long']]

    # Create a new csv file with formatted data
    df_expected.to_csv(f"{OUTPUT_PATH}{OUTPUT_FILE_NAME}.csv", index=False)
    # if args.last_run_file:
    #     df_expected.to_csv(f"{OUTPUT_PATH}{OUTPUT_FILE_NAME}_updated_from_last_run.csv",
    #                        index=False)
    # else:
    #     df_expected.to_csv(f"{OUTPUT_PATH}{OUTPUT_FILE_NAME}_new.csv",
    #                        index=False)

    datetime_str = datetime.datetime.now().strftime('%Y%m%d-%H%M%S')
    df_expected.to_csv(f"{OUTPUT_PATH}runs/{OUTPUT_FILE_NAME}_{datetime_str}.csv", index=False)

    if args.debug:
        print(
            f"The new file has been created at {OUTPUT_PATH}{OUTPUT_FILE_NAME}.csv")
        #
        # if args.last_run_file != '':
        #     print(
        #         f"The new file has been created at {OUTPUT_PATH}{OUTPUT_FILE_NAME}_updated_from_last_run.csv")
        # else:
        #     print(
        #         f"The new file has been created at {OUTPUT_PATH}{OUTPUT_FILE_NAME}_new.csv")

        print(f"Excecution time: {time.time() - start} seconds")
